import {
  ADD_PRODUCT, REMOVE_PRODUCT, ORDER
} from '../actions/actionType';

let initialState = {
  list: [],
  total: 0,
}

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_PRODUCT:{
      let newList = [...state.list];
      const index = newList.findIndex(p => p.id === action.payload.id);
      if (index>=0) {
        newList[index].quantity += 1;
      } else {
        action.payload.quantity =1;
        newList.push(action.payload);
      }
      const newTotal = state.total + action.payload.price;
      return {
        ...state,
        total: newTotal,
        list: newList,
      };
    }
    case REMOVE_PRODUCT: {
      const newList = [...state.list];
      let newTotal = state.total;
      const index = newList.findIndex(p => p.id === action.payload.id);
      if (index >= 0) {
        newList.splice(index,1);
        newTotal = newTotal - (action.payload.price * action.payload.quantity);
        action.payload.quantity = 1;
      }
      return {
        ...state,
        total: newTotal,
        list: newList,
      };
    }
    case ORDER: {
      return {
        ...state,
        list: [],
        total: 0,
      };
    }
    default:
      return state;
  }
}

export default cartReducer;