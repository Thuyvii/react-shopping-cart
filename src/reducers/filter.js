import {UPDATE_SIZE} from '../actions/actionType'

let initialState = {
  size: []
}

const filterReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_SIZE: {
      return {
        size: action.payload,
      };
    }
    
    default:
      return state;
  }
}

export default filterReducer;