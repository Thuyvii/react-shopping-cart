import { combineReducers } from "redux";
import cartReducer from "./cart";
import filterReducer from "./filter";
import productReducer from "./product";
import sortReducer from "./sort";

  const rootReducer = combineReducers({
    product: productReducer,
    cart: cartReducer,
    filter: filterReducer,
    sort: sortReducer
  });

  export default rootReducer;