import {UPDATE_SORT} from '../actions/actionType'

let initialState = {
  type: ''
}

const sortReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_SORT: {
      return {
        type: action.payload,
      };
    }
    
    default:
      return state;
  }
}

export default sortReducer;