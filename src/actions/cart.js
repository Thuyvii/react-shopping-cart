import {
  ADD_PRODUCT, REMOVE_PRODUCT, ORDER
} from "./actionType";

export const addProduct = (product) =>  (dispatch) => {
    dispatch({
      type: ADD_PRODUCT,
      payload: product
    });
}

export const removeProduct = (product) =>  (dispatch) => {
  dispatch({
    type: REMOVE_PRODUCT,
    payload: product
  });
}

export const order = () =>  (dispatch) => {
  dispatch({
    type: ORDER,
  });
}