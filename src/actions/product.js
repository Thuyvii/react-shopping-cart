import axios from "axios";
import {
  FETCH_PRODUCTS_FAILURE,
  FETCH_PRODUCTS_REQUEST,
  FETCH_PRODUCTS_SUCCESS
} from "./actionType";

// export const fecthProductRequest = () => {
//   return {
//     type: FETCH_PRODUCTS_REQUEST,
//   };
// };

// const fecthProductSuccess = (products) => {
//   return {
//     type: FETCH_PRODUCTS_SUCCESS,
//     payload: products,
//   };
// };

// const fecthProductFailure = (error) => {
//   return {
//     type: FETCH_PRODUCTS_FAILURE,
//     payload: error,
//   };
// };

const compare = {
  asc: (a, b) => {
    if (a.price < b.price) return -1;
    if (a.price > b.price) return 1;
    return 0;
  },
  desc: (a, b) => {
    if (a.price > b.price) return -1;
    if (a.price < b.price) return 1;
    return 0;
  }
};

export const fetchProducts = (sort, size) => async (dispatch) => {
  dispatch({ type: FETCH_PRODUCTS_REQUEST, loading: true });
  try {
    let response = await axios.get(
      "https://60190360971d850017a4082e.mockapi.io/shopping-cart"
    );

    if (!!size && size.length > 0) {
      response.data = response.data.filter(p => {
        return (size.find(filterSize => filterSize === p.size)); 
      }
      );
    }

    if (!!sort) {
      response.data = response.data.sort(compare[sort]);
    }



    dispatch({ type: FETCH_PRODUCTS_SUCCESS, payload: response.data,  });
  } catch (error) {
    dispatch({ type: FETCH_PRODUCTS_FAILURE, error });
  }
};


