import {UPDATE_SORT} from './actionType'

export const updateSort = (type) =>  (dispatch) => {
  dispatch({
    type: UPDATE_SORT,
    payload: type
  });
}

