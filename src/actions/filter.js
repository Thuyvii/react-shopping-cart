import {UPDATE_SIZE} from './actionType'

export const updateSize = (size) =>  (dispatch) => {
  dispatch({
    type: UPDATE_SIZE,
    payload: size
  });
}

