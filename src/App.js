import "./App.css";
import Cart from "./components/cart/productList";
import Filter from "./components/filter";
import ListProduct from "./components/product/productList";

function App() {

  return (
    <div>
      <br />
      <br />
      <div className="container text-center">
        <div className="row">
          <div className="col-sm-2 text-left">
            {/* <strong>Size</strong>

            <div className="checkbox">
              <p>
                <input type="checkbox" value="S" />S
              </p>
              <p>
                <input type="checkbox" value="M" />M
              </p>
              <p>
                <input type="checkbox" value="L" />L
              </p>
            </div> */}
            <Filter />
          </div>

          {/* <div className="col-sm-10">
            <div className="row">
              <div className="col-sm-3">
                <p>16 Product(s) found</p>
              </div>
              <div className="col-sm-9">
                <div className="row">
                  <div className="col-sm-6"></div>
                  <div className="col-sm-2">
                    <p>Order by</p>
                  </div>

                  <div className="col-sm-4">
                    <select
                      name="sort"
                      id="inputsort"
                      className="form-control"
                      required="required"
                    >
                      <option value="">Select</option>
                      <option value="">Hightest to lowest</option>
                      <option value="">Lowest to highest</option>
                    </select>
                  </div>
                </div>
              </div>
              <Sort/>
            </div> */}

            <ListProduct />
          {/* </div> */}
        </div>
      </div>

      <div className="clearfix"></div>
      <Cart/>
    </div>
  );
}

export default App;
