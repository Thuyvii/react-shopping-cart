import React from 'react';
import { updateSort } from "../../actions/sort";
import { useDispatch } from 'react-redux';

function Sort() {
  const dispatch = useDispatch();

  const handleOnChangeSort = (e) => {
    dispatch(updateSort(e.target.value));
  }

  return (
    <div>
      <div className="col-sm-9">
        <div className="row">
          <div className="col-sm-6"></div>
          <div className="col-sm-2">
            <p>Order by</p>
          </div>

          <div className="col-sm-4">
          <select
                name="sort"
                id="inputsort"
                className="form-control"
                required="required"
                onChange={handleOnChangeSort}
              >
                <option value="">Select</option>
                <option value="desc">Hightest to lowest</option>
                <option value="asc">Lowest to highest</option>
              </select>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Sort;