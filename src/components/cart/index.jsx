import React from 'react';
import './style.scss';
import {removeProduct} from '../../actions/cart'
import { useDispatch } from 'react-redux';

function Cart({product}) {
  const dispatch = useDispatch();
  return (
        <div className="modal-body ">
          <div className="container">
            <div className="row">
              <div className="col-sm-3">
                <img src={product.image} alt="shirt" className="image" />
              </div>
              <div className="col-sm-4">
                <strong>{product.name}</strong>
                <p>Quantity: {product.quantity}</p>
              </div>
              <div className="col-sm-3">{product.price}</div>
              <div className="col-sm-2">
                <button className="btn-danger" onClick={()=> dispatch(removeProduct(product))}>Delete</button>
              </div>
            </div>
          </div>
        </div>
  )
}

export default Cart;