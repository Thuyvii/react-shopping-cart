import React, { useState } from 'react';
import Modal from "react-modal";
import { useDispatch, useSelector } from 'react-redux';
import { order } from '../../actions/cart';
import Cart from './index';
import './style.scss';

Modal.setAppElement("#root");

function ProductCart() {
  const [isOpenCart, setIsOpenCart] = useState(false);
  const state = useSelector((state) => state.cart);
  const dispatch = useDispatch();

  const renderProducts = () => {
    if (state.loading) {
      return <h1>Loading...</h1>
    }
    return (
      <div>
        {
          state.list.map((product) => (
            <Cart product={product} key={product.id} />
          ))
        }
      </div>
    )
  }

  const handleCart = () => {
    if (state.list.length)
      return (
        <div className="scroll-content">
          {renderProducts()}
        </div>
      )
    return (
      <div>
        <h1 className="empty-cart">Empty</h1>
      </div>
    )
  }

  const handleOrder = () => {
    if (state.list.length) {
      dispatch(order());
      alert("Done");
    } else {
      alert('Add some product in the bag!')
    }
  }


  return (
    <div className="float-cart">
      <span className="bag bag--float-cart-closed" onClick={() => setIsOpenCart(true)} >
        <span className="bag__quantity">{state.list.length}</span>
      </span>
      <Modal
        isOpen={isOpenCart}
        shouldCloseOnOverlayClick={false}
        onRequestClose={() => setIsOpenCart}
        className="modal-content"
      >
        <div className="modal-header">
          <span className="close" onClick={() => setIsOpenCart(false)}>
            &times;
            </span>
          <h2><strong>Cart</strong></h2>
        </div>
        {handleCart()}

        <div className="container">
          <div className="row total">
            <div className="col-sm-9">
              <h3>Total</h3>
            </div>
            <div className="col-sm-3">
              <h2>$ {state.total}</h2>
            </div>
          </div>
        </div>

        <div className="modal-footer">
          <button className="btn-danger" onClick={handleOrder}>
            Order
            </button>
        </div>
      </Modal>

      

    </div>


  )
}

export default ProductCart;