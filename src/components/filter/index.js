import React from "react";
import { useDispatch } from "react-redux";
import { updateSize } from "../../actions/filter";
import './style.css'

function Filter() {
  const dispatch = useDispatch();
  const selectedCheckboxes = new Set();
  function toggleCheckbox(label) {
    if (selectedCheckboxes.has(label)) {
      selectedCheckboxes.delete(label);
    } else {
      selectedCheckboxes.add(label);
    }
    dispatch(updateSize(Array.from(selectedCheckboxes)));
  }
  return (
    <div>
      <strong>Size</strong>

      <form>
        <div className="checkbox">
          <label>
            <input type="checkbox" value="S" onClick={()=>toggleCheckbox("S")} />S
          </label>
          <br/>
          <label>
            <input type="checkbox" value="M" onClick={()=>toggleCheckbox("M")} />M
          </label>
          <br/>
          <label>
            <input type="checkbox" value="L" onClick={()=>toggleCheckbox("L")} />L
          </label>
        </div>
      </form>
    </div>
  );
}

export default Filter;
