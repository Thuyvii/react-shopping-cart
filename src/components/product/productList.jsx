import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchProducts } from '../../actions/product';
import Sort from '../sort';
import Product from './index';



function ProductList() {
  const dispatch = useDispatch();
  const productState = useSelector((productState) => productState.product);
  const sizeState = useSelector((sizeState) => sizeState.filter.size);
  const sortState = useSelector((sort) => sort.sort.type);

  const renderProducts = () => {
    if (productState.loading) {
      return <h1>Loading...</h1>
    }
    return (
      <div>
        {
          productState.products.map((product) => (
            <Product product={product} key={product.id} />
          ))
        }
      </div>
    )
  }

  useEffect(() => {
    dispatch(fetchProducts(sortState, sizeState));
  }, [sortState, sizeState, dispatch]);


  return (
    <div className="col-sm-10">
      <div className="row">
        <div className="col-sm-3">
          <p><strong>{productState.products.length}</strong> Product(s) found</p>
        </div>
        <Sort />

      </div>
      <div className="row">
        <div className="panel">
          <div className="panel-body">
            <div className="row">
              {renderProducts()}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};



export default ProductList