import React from 'react';
import { useDispatch } from 'react-redux';
import { addProduct } from '../../actions/cart';

function Product({product}) {
  const dispatch = useDispatch();
  return (
    <div className="col-sm-4 product">
      <div className="tag-ship">Freeship</div>
      <img
        src={product.image}
        className="img-responsive"
        alt="product"
      />
      <br />
      <label>{product.name}</label>
      <br />
      <strong className="text-center danger price">
        $ {product.price}
      </strong>
      <div className="btn btn-info" onClick={()=> dispatch(addProduct(product))}>Add to cart</div>
          
    </div>
  );
}

export default Product;